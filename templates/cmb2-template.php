<?php
// hook the function to the cmb2_init action
add_action( 'cmb2_init', 'add_meta_boxes' );
add_action( 'cmb2_init', 'add_meta_boxes_groups' );

/**
 * hook into WP's add_meta_boxes action hook
 */
function add_meta_boxes() {
	// set the prefix (start with an underscore to hide it from the custom fields list
	$prefix = '_my_prefix_';

	// create the metabox
	$cmb = new_cmb2_box( array(
		'id'            => 'test_metabox',
		'title'         => 'Test Metabox',
		'object_types'  => array( 'post-type-template' ), // post type
		'context'       => 'normal', // 'normal', 'advanced' or 'side'
		'priority'      => 'high', // 'high', 'core', 'default' or 'low'
		'show_names'    => true, // show field names on the left
		'cmb_styles'    => false, // false to disable the CMB stylesheet
		'closed'        => false, // keep the metabox closed by default
	) );

	// Regular text field
	$cmb->add_field( array(
		'name'       => __( 'Test Text', 'cmb2' ),
		'desc'       => __( 'field description (optional)', 'cmb2' ),
		'id'         => $prefix . 'text',
		'type'       => 'text',
		'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
		// 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
		// 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
		// 'on_front'        => false, // Optionally designate a field to wp-admin only
		// 'repeatable'      => true,
	) );

	// URL text field
	$cmb->add_field( array(
		'name' => __( 'Website URL', 'cmb2' ),
		'desc' => __( 'field description (optional)', 'cmb2' ),
		'id'   => $prefix . 'url',
		'type' => 'text_url',
		// 'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'), // Array of allowed protocols
		// 'repeatable' => true,
	) );

	// Email text field
	$cmb->add_field( array(
		'name' => __( 'Test Text Email', 'cmb2' ),
		'desc' => __( 'field description (optional)', 'cmb2' ),
		'id'   => $prefix . 'email',
		'type' => 'text_email',
		// 'repeatable' => true,
	) );	
} // END public function add_meta_boxes()

 
// create the function that creates metaboxes and populates them with fields
function add_meta_boxes_groups() {
     
    // set the prefix (start with an underscore to hide it from the custom fields list
    $prefix = '_my_prefix_';

    // create the metabox
    $cmb = new_cmb2_box( array(
        'id'            => 'test_metabox_group',
        'title'         => 'Test Metabox Group',
        'object_types'  => array( 'post-type-template' ), // post type
        'context'       => 'normal', // 'normal', 'advanced' or 'side'
        'priority'      => 'high', // 'high', 'core', 'default' or 'low'
        'show_names'    => true, // show field names on the left
        'cmb_styles'    => false, // false to disable the CMB stylesheet
        'closed'        => false, // keep the metabox closed by default
    ) );

	$group_field_id = $cmb->add_field( array(
	    'id'          => 'wiki_test_repeat_group',
	    'type'        => 'group',
	    'description' => __( 'Generates reusable form entries', 'cmb' ),
	    'options'     => array(
	        'group_title'   => __( 'Entry {#}', 'cmb' ), // since version 1.1.4, {#} gets replaced by row number
	        'add_button'    => __( 'Add Another Entry', 'cmb' ),
	        'remove_button' => __( 'Remove Entry', 'cmb' ),
	        'sortable'      => true, // beta
	        // 'closed'     => true, // true to have the groups closed by default
	    ),
	) );

	// Id's for group's fields only need to be unique for the group. Prefix is not needed.
	$cmb->add_group_field( $group_field_id, array(
	    'name' => 'Entry Title',
	    'id'   => 'title',
	    'type' => 'text',
	    // 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
	) );

	$cmb->add_group_field( $group_field_id, array(
	    'name' => 'Description',
	    'description' => 'Write a short description for this entry',
	    'id'   => 'description',
	    'type' => 'textarea_small',
	) );

	$cmb->add_group_field( $group_field_id, array(
	    'name' => 'Entry Image',
	    'id'   => 'image',
	    'type' => 'file',
	) );

	$cmb->add_group_field( $group_field_id, array(
	    'name' => 'Image Caption',
	    'id'   => 'image_caption',
	    'type' => 'text',
	) );
}

?>